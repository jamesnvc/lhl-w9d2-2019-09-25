//
//  ViewController.swift
//  ExtensionsDemo
//
//  Created by James Cash on 25-09-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

import UIKit
import NumbersSharedFramework

class ViewController: UIViewController {

    @IBOutlet weak var numberSlider: UISlider!
    @IBOutlet weak var numberLabel: UILabel!

    var theNumber: Double = 50

    override func viewDidLoad() {
        super.viewDidLoad()

        if let obj = UserDefaults(suiteName: suiteName)!.object(forKey: numberDefaultsKey),
            let num = obj as? Double {
            theNumber = num
        }
        numberLabel.text = "\(theNumber)"
        numberSlider.value = Float(theNumber)
    }

    @IBAction func numberChanged(_ sender: UISlider) {
        theNumber = Double(sender.value)
        numberLabel.text = "\(theNumber)"
        UserDefaults(suiteName: suiteName)!.set(theNumber, forKey: numberDefaultsKey)
    }
    
}

