//
//  TodayViewController.swift
//  NumbersToday
//
//  Created by James Cash on 25-09-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

import UIKit
import NotificationCenter
import NumbersSharedFramework

class TodayViewController: UIViewController, NCWidgetProviding {
        
    @IBOutlet weak var numbersLabel: UILabel!
    @IBOutlet weak var numberStepper: UIStepper!

    var theNumber: Double = 50

    override func viewDidLoad() {
        super.viewDidLoad()

        if let obj = UserDefaults(suiteName: suiteName)!.object(forKey: numberDefaultsKey),
            let num = obj as? Double {
            theNumber = num
        }
        numbersLabel.text = "\(theNumber)"
        numberStepper.value = theNumber
    }
    
    @IBAction func numberStepped(_ sender: UIStepper) {
        theNumber = sender.value
        numbersLabel.text = "\(theNumber)"
        UserDefaults(suiteName: suiteName)!.set(theNumber, forKey: numberDefaultsKey)
    }

    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        // Perform any setup necessary in order to update the view.
        
        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData
        if let obj = UserDefaults(suiteName: suiteName)!.object(forKey: numberDefaultsKey),
            let num = obj as? Double {
            theNumber = num
        }
        numbersLabel.text = "\(theNumber)"
        numberStepper.value = theNumber
        
        completionHandler(NCUpdateResult.newData)
    }
    
}
