//
//  ShareViewController.swift
//  NumbersShare
//
//  Created by James Cash on 25-09-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

import UIKit
import Social

class ShareViewController: SLComposeServiceViewController {

    override func isContentValid() -> Bool {

        let nf = NumberFormatter()
        nf.allowsFloats = true
        nf.minimum = 0
        nf.maximum = 100

        if nf.number(from: contentText) != nil {
            return true
        }

        return false
    }

    override func didSelectPost() {
        // This is called after the user selects Post. Do the upload of contentText and/or NSExtensionContext attachments.

        print("Saving number")

        // Inform the host that we're done, so it un-blocks its UI. Note: Alternatively you could call super's -didSelectPost, which will similarly complete the extension context.
        self.extensionContext!.completeRequest(returningItems: [], completionHandler: nil)
    }

    override func configurationItems() -> [Any]! {
        // To add configuration options via table cells at the bottom of the sheet, return an array of SLComposeSheetConfigurationItem here.
        return []
    }

}
