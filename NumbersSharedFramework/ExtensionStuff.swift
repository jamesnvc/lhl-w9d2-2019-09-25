//
//  ExtensionStuff.swift
//  NumbersSharedFramework
//
//  Created by James Cash on 25-09-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

import Foundation

public let numberDefaultsKey = "com.occasionallycogent.theNumberDefaultsKey"
public let suiteName = "groups.occasionallycogent.testing"

public func getStoredNumber() -> Double? {
    if let obj = UserDefaults(suiteName: suiteName)!.object(forKey: numberDefaultsKey),
        let num = obj as? Double {
        return num
    }
    return nil
}

public func save(number: Double) {
    UserDefaults(suiteName: suiteName)!.set(number, forKey: numberDefaultsKey)
}
