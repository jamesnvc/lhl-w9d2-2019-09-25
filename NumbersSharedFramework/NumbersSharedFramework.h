//
//  NumbersSharedFramework.h
//  NumbersSharedFramework
//
//  Created by James Cash on 25-09-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for NumbersSharedFramework.
FOUNDATION_EXPORT double NumbersSharedFrameworkVersionNumber;

//! Project version string for NumbersSharedFramework.
FOUNDATION_EXPORT const unsigned char NumbersSharedFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <NumbersSharedFramework/PublicHeader.h>


